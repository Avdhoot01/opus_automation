package OpusAutomation.OpusAutomation;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

import lib.Locators;
import basicFactor.ExtentReportGenerator;
import basicFactor.GetRowNumberOnTC;
import basicFactor.ReadExcelDataUsingUtilClass;
import basicFactor.SetExcelPath;

public class CreateContactAddressPage {

	static String address_line_1 = null;
	static String zip_code = null;
	static boolean status =false;
	static String TestCase= null;
	static ExtentReportGenerator report_generator = new ExtentReportGenerator();
	
	
	public boolean new_customer_click(WebDriver driver, String TestCase, ExtentTest test, ExtentReports report) {
		this.TestCase = TestCase;
		
		
		int retry_count =5;


		
		for (int i = 0; i < retry_count ; i ++) {
			try {
				WebDriverWait wait=new WebDriverWait(driver, 50);
				String pageTitle = driver.getTitle();
				System.out.println("Title of the current page is : " + pageTitle);
				driver.switchTo().frame("VISIFRAME");

				WebElement new_customer_tab = wait.until(ExpectedConditions.elementToBeClickable(Locators.newCust));
				System.out.println(i);
				if (new_customer_tab.isEnabled() && new_customer_tab.isDisplayed()) {
					System.out.println(i);
					JavascriptExecutor js = (JavascriptExecutor) driver;
					String SS_name = report_generator.take_ScreenShot(driver, TestCase);
					test.info("New customaer tab clicked", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"
							+SS_name).build());
					System.out.println(i);
					Thread.sleep(5000);
					js.executeScript("arguments[0].click();", new_customer_tab);
					status = EnterAddress(driver,test, report);
					break;
				}
			}
			catch ( Exception e ) {
				status =false;
			}
		}
		return status;
	}


	public  static boolean EnterAddress(WebDriver driver, ExtentTest test, ExtentReports report ) throws Exception {
		try {

			driver.switchTo().defaultContent();
			driver.switchTo().frame("VISIFRAME");

			SetExcelPath path = new SetExcelPath();
			ReadExcelDataUsingUtilClass read_excel_data = new ReadExcelDataUsingUtilClass(path.return_excel_path());
			GetRowNumberOnTC testcase_row_number = new GetRowNumberOnTC();

			address_line_1 = read_excel_data.getCellData("TestCase", "address_line1",testcase_row_number.getRownumber(TestCase, "TestCase") );


			zip_code = read_excel_data.getCellData("TestCase", "zip_Code",testcase_row_number.getRownumber(TestCase, "TestCase") );

			Thread.sleep(4000);
			driver.findElement(Locators.address1).sendKeys(address_line_1);
			Thread.sleep(2000);
			driver.findElement(Locators.zip).sendKeys(zip_code);

			Thread.sleep(5000);
			
			String SS_name = report_generator.take_ScreenShot(driver, TestCase);
			test.info("Address set as expected", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+SS_name).build());
			
			WebElement checkAvail = driver.findElement(Locators.checkAvailabilty);
			JavascriptExecutor js1 = (JavascriptExecutor) driver;
			js1.executeScript("arguments[0].click();", checkAvail );
			status = true;
		}
		catch(Exception e) {
			status = false;
			test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+report_generator.take_ScreenShot(driver, TestCase)).build());
		}
		return status;
	}
}
