package OpusAutomation.OpusAutomation;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

import basicFactor.ExtentReportGenerator;
import basicFactor.GetRowNumberOnTC;
import basicFactor.ReadExcelDataUsingUtilClass;
import basicFactor.SetExcelPath;



public class WirelessOperations {

	String Testcase;
	SetExcelPath path = new SetExcelPath();
	ReadExcelDataUsingUtilClass read_excel_data = new ReadExcelDataUsingUtilClass(path.return_excel_path());
	GetRowNumberOnTC testcase_row_number = new GetRowNumberOnTC();
	BasicOperation boperation = new BasicOperation(Testcase);
	ExtentReports report;
	ExtentTest  test;
	boolean status ;
	ExtentReportGenerator report_generator = new ExtentReportGenerator();
	
	public WirelessOperations(String TestCase) throws Exception {
		Testcase=TestCase;
		System.out.print("Testcase name --> "+Testcase);
	}

	
	BasicOperation basic_operation = new BasicOperation(Testcase);
	
	public void switch_toWireless(WebDriver driver) {
		@SuppressWarnings("deprecation")
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(80000, TimeUnit.MILLISECONDS) 			
					.pollingEvery(1000, TimeUnit.MILLISECONDS) 			
				.ignoring(NoSuchElementException.class);
		
		
		WebElement clickInternet = wait.until(new Function<WebDriver, WebElement>(){
			
			public WebElement apply(WebDriver driver ) {
				return driver.findElement(By.xpath("//*[@id='wirelessOffer']"));
			}
		});
				
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", clickInternet);
		System.out.println("Wireless section clicked");
	}

	
	public boolean Click_Phone_Plus_Button(WebDriver driver, ExtentTest  test) throws Exception {
		
	String phone_plus_count=read_excel_data.getCellData("TestCase", "Phone Count",testcase_row_number.getRownumber(Testcase,"TestCase" ) );
	System.out.print(phone_plus_count);
	
	try {
		for (int i =0; i<Integer.parseInt(basic_operation.trim_digit(phone_plus_count)); i++) {
			
			WebDriverWait wait = new WebDriverWait(driver, 350);
//			Thread.sleep(80000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mixAndMatchDevice-phone\"]/div[1]/div[2]/div[3]/div")));
			WebElement click_plus_button = driver.findElement(By.xpath("//*[@id=\"mixAndMatchDevice-phone\"]/div[1]/div[2]/div[3]/div"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", click_plus_button);	
			
			test.info(phone_plus_count+" time phone count clicked", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
			click_lets_go(driver, test);
			status = true;
		}
	}
	catch (Exception e) 
	{
		status = false;
		test.fail(e.getLocalizedMessage(), MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
				report_generator.take_ScreenShot(driver, Testcase)).build());
	}
			
		return status;
	}
	
	public void click_lets_go(WebDriver driver, ExtentTest  test) throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 350);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[.=\"Let's go\"]")));
			WebElement next = driver.findElement(By.xpath("//span[.=\"Let's go\"]"));
//			Thread.sleep(2000);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", next);
			test.info("Let's go button clicked", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
		}
		catch (Exception e) 
		{

			test.fail(e.getLocalizedMessage(), MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
		}
	
	}
	
	public boolean enter_customer_details(WebDriver driver , ExtentTest  test) throws Exception{
		
		try{
			
				WebDriverWait wait = new WebDriverWait(driver, 350);
			
				
					String firstname= read_excel_data.getCellData("CustomerDetails", "First Name",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
					System.out.println(firstname);
					
					String lastname= read_excel_data.getCellData("CustomerDetails", "Last Name",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
					System.out.println(lastname);
					System.out.print("Waiting here to load text properly");
					
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder =\"*First Name\"]")));
					Thread.sleep(30000);
					driver.findElement(By.xpath("//input[@placeholder =\"*First Name\"]")).sendKeys(firstname);
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder =\"*Last Name\"]")));
					driver.findElement(By.xpath("//input[@placeholder =\"*Last Name\"]")).sendKeys(lastname);
					
					WebElement next=driver.findElement(By.xpath("//div[contains(text(),'Next')]"));
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].click();", next);
					
					test.info("Customer details added", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
							report_generator.take_ScreenShot(driver, Testcase)).build());
					status = true;
			
			}
			
		catch (Exception e) {
			status = false;
			test.fail(e.getLocalizedMessage(), MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
		}
		
		return status;
		
		
//		Thread.sleep(8000);
	}
	
	public boolean Device_Selection(WebDriver driver , ExtentTest  test) throws Exception{
		System.out.println("Device selection");
		try {
			WebDriverWait wait = new WebDriverWait(driver, 420);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Deliver it')]")));
			
			
			WebElement deliver_it = driver.findElement(By.xpath("//span[contains(text(),'Deliver it')]"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", deliver_it);
			
//			Thread.sleep(2000);
			test.info("Customer details entered", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
		
			String device_name =read_excel_data.getCellData("TestCase", "Device Name",testcase_row_number.getRownumber(Testcase, "TestCase") );
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='mix-n-match-device-card-name' and .=\""+device_name+"\"]")));
			WebElement SelectDevice=driver.findElement(By.xpath("//span[@class='mix-n-match-device-card-name' and .=\""+device_name+"\"]"));
			js.executeScript("arguments[0].click();", SelectDevice);
//			Thread.sleep(10000);
			
			
			test.info("Device selected", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
			
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@id,\"mixAndMatchDFAddDevice\")]")));
			WebElement add_device= driver.findElement(By.xpath("//div[contains(@id,\"mixAndMatchDFAddDevice\")]"));
			js.executeScript("arguments[0].click();", add_device);
//			Thread.sleep(5000);
	
			
			test.info("Device  added", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
			
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/label[contains(text(),\""+read_excel_data.getCellData("TestCase", "Insurance Type",testcase_row_number.getRownumber(Testcase, "TestCase") )+"\")]")));
			WebElement decline_radio_button = driver.findElement(By.xpath("//div/label[contains(text(),\""+read_excel_data.getCellData("TestCase", "Insurance Type",testcase_row_number.getRownumber(Testcase, "TestCase") )+"\")]"));
			js.executeScript("arguments[0].click();", decline_radio_button);
			
			test.info("Insurence added", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
			status = true;
			
		}
		catch (Exception e) {
			status = false;
			test.fail(e.getLocalizedMessage(), MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
		}
	return status;
	}
	
	
	public boolean SelectPricePlan(WebDriver driver, ExtentTest  test) throws Exception{
//		Thread.sleep(4000);
		try {
			System.out.println("Price plan selection");
			JavascriptExecutor js = (JavascriptExecutor) driver;
			
			WebDriverWait wait = new WebDriverWait(driver, 420);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@id,\"mixAndMatchSelectPlanLink\")]")));
			WebElement select = driver.findElement(By.xpath("//div[contains(@id,\"mixAndMatchSelectPlanLink\")]"));
			js.executeScript("arguments[0].click();", select);
//			Thread.sleep(5000);
			
			
			String Price_Plane_name =read_excel_data.getCellData("TestCase", "WLS Price Plan",testcase_row_number.getRownumber(Testcase, "TestCase") );
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),\""+Price_Plane_name+"\")]/..//button[@id=\"buttonId\"]")));
			WebElement pp = driver.findElement(By.xpath("//div[contains(text(),\""+Price_Plane_name+"\")]/..//button[@id=\"buttonId\"]"));
			
			js.executeScript("arguments[0].click();", pp);
			
//			Thread.sleep(5000);
			
			test.info("Price Plan selected", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//*[@class='mix-n-match-btn-regular mnm-button-blue'])[1]")));
			WebElement Done_add_line = driver.findElement(By.xpath("(//*[@class='mix-n-match-btn-regular mnm-button-blue'])[1]"));
			js.executeScript("arguments[0].click();", Done_add_line);
			
			test.info("Done add line clicked", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
		status = true;
		}
		
		catch (Exception e) {
			status = false;
			test.fail(e.getLocalizedMessage(), MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
		}
		
	return status;
	}
	
	
	
	
}
