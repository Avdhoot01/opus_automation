package OpusAutomation.OpusAutomation;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

import lib.Locators;
import basicFactor.ExtentReportGenerator;
import basicFactor.GetRowNumberOnTC;
import basicFactor.ReadExcelDataUsingUtilClass;
import basicFactor.SetExcelPath;




public class Hsia_Basic_Operation {
	String Testcase;	
	boolean status;
	
	SetExcelPath path = new SetExcelPath();
	ReadExcelDataUsingUtilClass read_excel_data = new ReadExcelDataUsingUtilClass(path.return_excel_path());
	GetRowNumberOnTC testcase_row_number = new GetRowNumberOnTC();
	ExtentReportGenerator report_generator = new ExtentReportGenerator();
	
	public Hsia_Basic_Operation(String TestCase) throws Exception {
		Testcase=TestCase;
	}
	
	
	public void switch_to_internet(WebDriver driver) throws Exception{
	
		@SuppressWarnings("deprecation")
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(80000, TimeUnit.MILLISECONDS) 			
					.pollingEvery(1000, TimeUnit.MILLISECONDS) 			
				.ignoring(NoSuchElementException.class);
		
		
		WebElement clickInternet = wait.until(new Function<WebDriver, WebElement>(){
			
			public WebElement apply(WebDriver driver ) {
				return driver.findElement(By.xpath("//*[@id='internetOffer']"));
			}
		});
				
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", clickInternet);
		System.out.println("Internet section clicked");
	}
	
	public boolean select_Internet_package(WebDriver driver,  ExtentTest  test,  ExtentReports report) throws Exception{
		
	
		
		String InternetPlan = read_excel_data.getCellData("TestCase", "Internet Plan",testcase_row_number.getRownumber(Testcase,"TestCase" ) );
		String all_included = read_excel_data.getCellData("TestCase", "Internet Plan Included",testcase_row_number.getRownumber(Testcase, "TestCase") );
	
		if (InternetPlan.contains(".0")) {
			InternetPlan = InternetPlan.substring(0, InternetPlan.length()-2);
		}
		
		final String InternetPlan_ = InternetPlan;
		
		test.info("Switch to Internet",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
				report_generator.take_ScreenShot(driver, Testcase)).build() );
//		
//		@SuppressWarnings("deprecation")
//		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
//				.withTimeout(80000, TimeUnit.MILLISECONDS) 			
//					.pollingEvery(1500, TimeUnit.MILLISECONDS) 			
//				.ignoring(NoSuchElementException.class);
		WebDriverWait wait = new WebDriverWait(driver,200);
		WebElement selectHSIApackage;
		try {
			if ( all_included.equalsIgnoreCase("Y")) {
				

//				WebElement selectHSIApackage = wait.until(new Function<WebDriver, WebElement>(){
//					
//					public WebElement apply(WebDriver driver ) {
//						return driver.findElement(By.xpath("//input[@id=\"productSelectedOfferForHsi\" and contains(@selectedhsiofferid,\""+InternetPlan_+"\")]"));
//					}
//				});
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id=\"productSelectedOfferForHsi\" and contains(@selectedhsiofferid,\""+InternetPlan_+"\")]")));			
			
				selectHSIApackage=driver.findElement(By.xpath("//input[@id=\"productSelectedOfferForHsi\" and contains(@selectedhsiofferid,\""+InternetPlan_+"\")]"));
				System.out.println("Before select");
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].click();", selectHSIApackage);
				
				test.info(selectHSIApackage +" Internet Plan selected",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, Testcase)).build() );
				
				
			}
			else {
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id=\"productSelectedOfferForHsi\" and contains(@selectedhsiofferid,\""+InternetPlan_+"\") and and contains(@selectedhsiofferid, 'RATE')]")));			
				
				selectHSIApackage=driver.findElement(By.xpath("//input[@id=\"productSelectedOfferForHsi\" and contains(@selectedhsiofferid,\""+InternetPlan_+"\") and and contains(@selectedhsiofferid, 'RATE')]"));
//				WebElement selectHSIApackage = wait.until(new Function<WebDriver, WebElement>(){
//					
//					public WebElement apply(WebDriver driver ) {
//						return driver.findElement(By.xpath("//input[@id=\"productSelectedOfferForHsi\" and contains(@selectedhsiofferid,\""+InternetPlan_+"\") and and contains(@selectedhsiofferid, 'RATE')]"));
//					}
//				});

				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].click();", selectHSIApackage);
				System.out.println(InternetPlan+" selected");
				
				test.info(selectHSIApackage +" Internet Plan selected",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, Testcase)).build() );
			}
			status = true;
		}
		catch (Exception e) 
		{
			
			status =false;
			
			test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build() );
			
		}
		return status;
	}
		

}
