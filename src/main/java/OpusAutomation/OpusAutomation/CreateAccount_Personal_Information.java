package OpusAutomation.OpusAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

import basicFactor.ExtentReportGenerator;
import basicFactor.GetRowNumberOnTC;
import basicFactor.ReadExcelDataUsingUtilClass;
import basicFactor.SetExcelPath;
import lib.Locators;

public class CreateAccount_Personal_Information {

	String Testcase;
	SetExcelPath path = new SetExcelPath();
	
	public CreateAccount_Personal_Information(String TestCase) throws Exception {
		Testcase=TestCase;
	}

	ReadExcelDataUsingUtilClass read_excel_data = new ReadExcelDataUsingUtilClass(path.return_excel_path());
	GetRowNumberOnTC testcase_row_number = new GetRowNumberOnTC();
	BasicOperation boperation = new BasicOperation(Testcase);
	ExtentReports report;
	ExtentTest  test;
	boolean status ;
	ExtentReportGenerator report_generator = new ExtentReportGenerator();
	
	
	public boolean setTextAccountHolderInformation(WebDriver driver, ExtentTest test) throws Exception {
	
		String first_name=read_excel_data.getCellData("CustomerDetails", "First Name",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
		String last_name =read_excel_data.getCellData("CustomerDetails", "Last Name",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
		String Date_Of_Month =read_excel_data.getCellData("CustomerDetails", "Date Of Birth",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
		
		String[] words=Date_Of_Month.split("/");
		String bMM = words[0];
		String bDD = words[1];
		String bYYYY = words[2];
		
		System.out.println(words);
		
		String SSN = read_excel_data.getCellData("CustomerDetails", "SSN",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
		
		String License_state =read_excel_data.getCellData("CustomerDetails", "License State",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
		String License_number =read_excel_data.getCellData("CustomerDetails", "License Number",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
		String License_date =read_excel_data.getCellData("CustomerDetails", "License Date",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
		
		String[] words_l=License_date.split("/");
		String lMM = words_l[0];
		String lDD = words_l[1];
		String lYYYY = words_l[2];
		
		System.out.println(words_l);
		
		try {
			WebDriverWait wait = new WebDriverWait(driver, 400);
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.fname));
			
			driver.findElement(Locators.fname).sendKeys(first_name);
			driver.findElement(Locators.lname).sendKeys(last_name);
			driver.findElement(Locators.cdobmonth).sendKeys(bMM);
			driver.findElement(Locators.cdobday).sendKeys(bDD);
			driver.findElement(Locators.cdobyear).sendKeys(bYYYY);
			driver.findElement(Locators.cSSN).sendKeys(SSN);
			driver.findElement(Locators.confirmcSSN).sendKeys(SSN);

			Select lstate = new Select(driver.findElement(Locators.license_state));
			lstate.selectByVisibleText(License_state);
			
			driver.findElement(Locators.license_number).sendKeys(License_number);
			driver.findElement(Locators.licensemonth).sendKeys(lMM);
			driver.findElement(Locators.licenseday).sendKeys(lDD);
			driver.findElement(Locators.licenseyear).sendKeys(lYYYY);
			
			test.info("All details entered ", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build() );
			
			
			WebElement submit_button = driver.findElement(Locators.sbutton);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", submit_button);
			System.out.println("clicked");
			status = true;
			
		}
		catch(Exception e) {
			test.fail(e);
			status = false;
			e.printStackTrace();
		}
		
		return status;
	}
	
	
	
	public boolean Account_Information(WebDriver driver, ExtentTest test) throws Exception{
		try {
			WebDriverWait wait = new WebDriverWait(driver, 400);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='taxExempt' and @value='N']")));
			
			WebElement textaxExempt = driver.findElement(By.xpath("//input[@name='taxExempt' and @value='N']"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", textaxExempt);
			test.info("taxExempt set to No", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
			status = true;
		}
		catch (Exception e) {
			status = false;
			test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
		}
		return status;
	}
	
	
	public boolean Security_Information(WebDriver driver, ExtentTest test) throws Exception{
		try {
			WebDriverWait wait = new WebDriverWait(driver, 400);
			wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.securityPasscode));
			
			String Security_Passcode = read_excel_data.getCellData("CustomerDetails", "Security Passcode",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
			String Security_Answer = read_excel_data.getCellData("CustomerDetails", "Security  Answer",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
			
			
			WebElement Passcode = driver.findElement(Locators.securityPasscode);
			WebElement Sec_Ans = driver.findElement(Locators.securityAnswer);
			
			Passcode.sendKeys(Security_Passcode);
			
			Sec_Ans.sendKeys(Security_Answer);
			test.info("Security information updated",  MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
			status = true;
		}
		catch (Exception e) {
			status = false;
			test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
		}
		return status;
		
	}
	
	public boolean Contact_Information(WebDriver driver, ExtentTest test) throws Exception{
		try {
			WebDriverWait wait = new WebDriverWait(driver, 400);
			wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.primaryPhone));
			
			String Primary_Phone = read_excel_data.getCellData("CustomerDetails", "Primary Phone",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
			driver.findElement(Locators.primaryPhone).sendKeys(Primary_Phone);
			
			String Primary_phone_type =read_excel_data.getCellData("CustomerDetails", "Primary Phone Type",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
			Select PP_type = new Select(driver.findElement(Locators.primaryPhoneType));
			PP_type.selectByVisibleText(Primary_phone_type);
			
			
			String alt_ph =read_excel_data.getCellData("CustomerDetails", "Alternate Phone",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
			driver.findElement(Locators.secPhone).sendKeys(alt_ph);
			
			String alt_type = read_excel_data.getCellData("CustomerDetails", "Alternate Phone Type",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
			Select altP_type = new Select(driver.findElement(Locators.alternativePhoneType));
			altP_type.selectByVisibleText(alt_type);
			
			String email = read_excel_data.getCellData("CustomerDetails", "Email",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
			driver.findElement(By.xpath("//input[@id = 'email_emailId']")).sendKeys(email);
			
			String contact_mode =read_excel_data.getCellData("CustomerDetails", "Contact Method",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
			Select CM = new Select(driver.findElement(Locators.prefContactMethod));
			CM.selectByVisibleText(contact_mode);
			
			String contact_time = read_excel_data.getCellData("CustomerDetails", "Contact Time",testcase_row_number.getRownumber(Testcase, "CustomerDetails") );
			Select CT = new Select(driver.findElement(Locators.prefContactTime));
			CT.selectByVisibleText(contact_time);
			
			test.info("Contact Information updated", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
			
			WebElement Next = driver.findElement(By.xpath("//input[@id='btnNxt']"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", Next);
			
			test.info("Next button clicked", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
			status = true;
		}
		
		catch (Exception e) {
			status = false;
			test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
		}
		return status;
	}
}
