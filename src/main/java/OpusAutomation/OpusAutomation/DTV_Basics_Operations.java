package OpusAutomation.OpusAutomation;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

import lib.Locators;
import basicFactor.ExtentReportGenerator;
import basicFactor.GetRowNumberOnTC;
import basicFactor.ReadExcelDataUsingUtilClass;
import basicFactor.SetExcelPath;

public class DTV_Basics_Operations {
	String Testcase;
	SetExcelPath path = new SetExcelPath();
	ReadExcelDataUsingUtilClass read_excel_data = new ReadExcelDataUsingUtilClass(path.return_excel_path());
	GetRowNumberOnTC testcase_row_number = new GetRowNumberOnTC();
	BasicOperation boperation = new BasicOperation(Testcase);
	ExtentReports report;
	ExtentTest  test;
	boolean status ;
	ExtentReportGenerator report_generator = new ExtentReportGenerator();

	public DTV_Basics_Operations(String TestCase) throws Exception {
		Testcase=TestCase;
	}

	public void Switch_TV(WebDriver driver) {
		@SuppressWarnings("deprecation")
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(80000, TimeUnit.MILLISECONDS) 			
					.pollingEvery(1000, TimeUnit.MILLISECONDS) 			
				.ignoring(NoSuchElementException.class);
		
		
		WebElement clickInternet = wait.until(new Function<WebDriver, WebElement>(){
			
			public WebElement apply(WebDriver driver ) {
				return driver.findElement(By.xpath("//*[@id='dtvOffer']"));
			}
		});
				
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", clickInternet);
		System.out.println("DTV section clicked");
	}


	public boolean selectDTVPackage(WebDriver driver, ExtentTest  test,  ExtentReports report) throws Exception{
		WebElement clickdtvpackage;
		try {

//			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
//					.withTimeout(85000, TimeUnit.MILLISECONDS) 			
//					.pollingEvery(1500, TimeUnit.MILLISECONDS) ;		
//					.ignoring(NoSuchElementException.class);


			final String DTV_Base_package=read_excel_data.getCellData("TestCase", "DTV_Base_Package",testcase_row_number.getRownumber(Testcase, "TestCase") );

			WebDriverWait wait = new WebDriverWait(driver,185);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@selectedofferid,\""+DTV_Base_package+"\")]")));
			clickdtvpackage = driver.findElement(By.xpath("//input[contains(@selectedofferid,\""+DTV_Base_package+"\")]"));

//			clickdtvpackage = wait.until(new Function<WebDriver, WebElement>(){
//
//				public WebElement apply(WebDriver driver ) {
//					return driver.findElement(By.xpath("//input[contains(@selectedofferid,\""+DTV_Base_package+"\")]"));
//				}
//			});

			
			//			Screenshot.captureScreenshot(driver, "DTV Card before selection2");

			test.info("Before Selecting DTV Base Package",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build() );
			Thread.sleep(2000);
			System.out.print("before");
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", clickdtvpackage);
			
			js.executeScript("arguments[0].click();", clickdtvpackage);
			
			System.out.println(DTV_Base_package+" selected");
			status = true;

		}
		catch (Exception e) 
		{
			status = false;
			test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build() );
		}
		return status;
	}


	public boolean number_of_tv_add(WebDriver driver,  ExtentTest  test,  ExtentReports report) throws Exception{
		final String number_of_tv=boperation.trim_digit(read_excel_data.getCellData("TestCase", "Number_of_TV",testcase_row_number.getRownumber(Testcase, "TestCase") ));
		System.out.println("number of Tv want to add ->"+number_of_tv);

		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(20000, TimeUnit.MILLISECONDS) 			
				.pollingEvery(15, TimeUnit.MILLISECONDS) 	;		
//				.ignoring(NoSuchElementException.class)

		try {

			WebElement number_of_tv_add;
			number_of_tv_add = wait.until(new Function<WebDriver, WebElement>(){

				public WebElement apply(WebDriver driver ) {
					return driver.findElement(By.xpath("//div[contains(text(),\""+"Number of TVs"+"\")]/..//a[@count=\""+number_of_tv+"\"]"));
				}
			});

			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", number_of_tv_add);
			js.executeScript("arguments[0].click();", number_of_tv_add);
			System.out.println("TVs addedd");
			status = true;
		}
		catch (Exception e) 
		{
			status = false;
//			test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(System.getProperty("user.dir")+"\\Report\\"
//					+Testcase+"\\Screenshots\\"+
//					report_generator.take_ScreenShot(driver, Testcase)).build() );
		}
		return status;
	}

	public boolean add_reciver(WebDriver driver, ExtentTest  test,  ExtentReports report) throws Exception{

		String STBtype="";
		String number_of_STB;
		WebDriverWait wait = new WebDriverWait(driver,10);

		STBtype=read_excel_data.getCellData("TestCase", "Equipment_type",testcase_row_number.getRownumber(Testcase, "TestCase") );

		System.out.println(STBtype);

		number_of_STB=boperation.trim_digit(read_excel_data.getCellData("TestCase", "No Of Equipment",testcase_row_number.getRownumber(Testcase, "TestCase") ));

		System.out.println(number_of_STB);

		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),\""+STBtype+"\")]/..//a[@count=\""+number_of_STB+"\"]")));
			WebElement selectSTBType = driver.findElement(By.xpath("//div[contains(text(),\""+STBtype+"\")]/..//a[@count=\""+number_of_STB+"\"]"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", selectSTBType);

			test.info("STB receiver added",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build() );

			System.out.println("Receiver addedd");
			status = true;
		}
		catch (Exception e) 
		{
			status = false;
//			test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(System.getProperty("user.dir")+"\\Report\\"
//					+Testcase+"\\Screenshots\\"+
//					report_generator.take_ScreenShot(driver, Testcase)).build());
		}
		return status;
	}


	public boolean add_protection_plan(WebDriver driver, ExtentTest  test,  ExtentReports report) throws Exception{


		String Plan_name=read_excel_data.getCellData("TestCase", "Protection_Plan",testcase_row_number.getRownumber(Testcase, "TestCase") );
		System.out.println("Pan name :> "+Plan_name);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[.=\""+Plan_name+"\"]//..//input[@type=\"button\"]")));
			WebElement selectPP = driver.findElement(By.xpath("//div[.=\""+Plan_name+"\"]//..//input[@type=\"button\"]"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", selectPP);
			js.executeScript("arguments[0].click();", selectPP);
			System.out.println("Protection Plan addedd");
			test.info(selectPP+" PP added",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build() );
			status = true;
		}
		catch (Exception e) 
		{
			status = false;
//			test.fail(e,MediaEntityBuilder.createScreenCaptureFromPath(System.getProperty("user.dir")+"\\Report\\"
//					+Testcase+"\\Screenshots\\"+
//					report_generator.take_ScreenShot(driver, Testcase)).build() );
		}
		return status;
	}

	//	public void autoPay_PaperLessBillcheckBoxHandle(WebDriver driver) throws Exception{
	//		String AutoPay_Check= read_excel_data.getCellData("TestCase", "AutoPay",testcase_row_number.getRownumber(Testcase, "TestCase") );
	//		String Paperless_Check = read_excel_data.getCellData("TestCase", "PaperlessBill",testcase_row_number.getRownumber(Testcase, "TestCase") );
	//		
	//		
	//		try {
	//			Thread.sleep(2000);
	//			Screenshot.captureScreenshot(driver, "Before Autopay handle");
	//			
	//			if (! AutoPay_Check.equals("Y")) {
	//				
	//				WebElement autoPay_checkBox = driver.findElement(By.xpath("//input[@name=\"autoBillPayOption\"]"));
	//				JavascriptExecutor js = (JavascriptExecutor) driver;
	//				js.executeScript("arguments[0].click();", autoPay_checkBox);
	//				System.out.println("Autopay uncheked");
	//				Screenshot.captureScreenshot(driver, "After Autopay handle");
	//			}
	//			
	//			if (! Paperless_Check.equals("Y")) {
	//				
	//				WebElement paperless_checkBox = driver.findElement(By.xpath("//input[@name=\"paperLessBillOption\"]"));
	//				JavascriptExecutor js = (JavascriptExecutor) driver;
	//				js.executeScript("arguments[0].click();", paperless_checkBox);
	//				System.out.println("Autopay uncheked");
	//				Screenshot.captureScreenshot(driver, "After papereless checkbox handle");
	//			}
	//			
	//		}
	//		
	//		catch (Exception e) 
	//		{
	//			
	//			e.printStackTrace();
	//		}
	//		
	//	}
	//	

	public void check_to_add_promotion(WebDriver driver, String check, ExtentTest  test) {

		try {
			for (int i =0; i< 3; i++) {
				System.out.println("retring -->" +i);
				WebDriverWait wait = new WebDriverWait(driver, 60);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[.=\"Add at no extra cost\"]")));
				WebElement val_msg = driver.findElement(By.xpath("//*[.=\"Add at no extra cost\"]"));
				if (val_msg.isDisplayed() && val_msg.isEnabled() ) {
					System.out.println("Add at no extra cost");
					add_EPIX_promo_dtv(driver,check);
					add_starz_showtime_promo_dtv(driver, check);
					add_HBO_MAX_promo_dtv(driver, check);

					WebElement continue_rtp_disclosure =  driver.findElement(By.xpath("//input[@id=\"rtpDisclosureOverlayContinueButton\"]"));
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].click();", continue_rtp_disclosure);
					break;
			}
			
			}
		}
		catch (Exception e) {

			System.out.println("Promo page not dispalyed");
		}
	}
	public void add_EPIX_promo_dtv(WebDriver driver,String check) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		try {
			if (check.equalsIgnoreCase("Y")) {
				WebElement EPIX = driver.findElement(By.xpath("//input[contains(@selectedofferid,\"DTV_BASE_PROMO\") and @value=\"Accept \"]"));
				js.executeScript("arguments[0].click();", EPIX);
				//				Screenshot.captureScreenshot(driver, "$5.99 off EPIX for 3 months added");
			}
			else {
				WebElement EPIX = driver.findElement(By.xpath("//input[contains(@selectedofferid,\"DTV_BASE_PROMO\") and @value=\"No Thanks\"]"));
				js.executeScript("arguments[0].click();", EPIX);
				//				Screenshot.captureScreenshot(driver, "$5.99 off EPIX for 3 months decliened");
			}
		}
		catch (Exception e) {

			System.out.println("$5.99 off EPIX for 3 months not available");
		}
	}


	public void add_starz_showtime_promo_dtv(WebDriver driver,String check) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		try {
			if (check.equalsIgnoreCase("Y")) {
				WebElement starz_showtime = driver.findElement(By.xpath("//input[contains(@selectedofferid,\"DTV_PICK_BASE_PROMO\") and @value=\"Accept \"]"));
				js.executeScript("arguments[0].click();", starz_showtime);
				//				Screenshot.captureScreenshot(driver, "$32.97 Off Starz, Showtime, and Cinemax for 3 months added");
			}
			else {
				WebElement starz_showtime = driver.findElement(By.xpath("//input[contains(@selectedofferid,\"DTV_PICK_BASE_PROMO\") and @value=\"No Thanks\"]"));
				js.executeScript("arguments[0].click();", starz_showtime);
				//				Screenshot.captureScreenshot(driver, "$32.97 Off Starz, Showtime, and Cinemax for 3 months decliened");
			}
		}
		catch (Exception e) {

			System.out.println("$32.97 Off Starz, Showtime, and Cinemax for 3 months not available");
		}
	}

	public void add_HBO_MAX_promo_dtv(WebDriver driver,String check) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		try {
			if (check.equalsIgnoreCase("Y")) {
				WebElement hbo_max = driver.findElement(By.xpath("//input[contains(@selectedofferid,\"DTV_PICK_HBO_ADD_PROMO\") and @value=\"Accept \"]"));
				js.executeScript("arguments[0].click();", hbo_max);
				//				Screenshot.captureScreenshot(driver, "$14.99 off HBO Max for 12 months added");
			}
			else {
				WebElement hbo_max = driver.findElement(By.xpath("//input[contains(@selectedofferid,\"DTV_PICK_HBO_ADD_PROMO\") and @value=\"No Thanks\"]"));
				js.executeScript("arguments[0].click();", hbo_max);
				//				Screenshot.captureScreenshot(driver, "$14.99 off HBO Max for 12 months decliened");
			}
		}
		catch (Exception e) {

			System.out.println("$14.99 off HBO Max for 12 months not available");
		}
	}

}
