package OpusAutomation.OpusAutomation;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

import lib.Locators;
import basicFactor.ExtentReportGenerator;
import basicFactor.GetRowNumberOnTC;
import basicFactor.ReadExcelDataUsingUtilClass;
import basicFactor.SetExcelPath;



public class ServiceProfileForNewCust {
	
	String dtv,hsia,iptv,voip,wls;
	boolean status;
	static ExtentReportGenerator report_generator = new ExtentReportGenerator();
	
	public boolean serviceAvailability(WebDriver driver, String TestCasename, ExtentTest test, ExtentReports report) throws Exception {
		
		
		
		SetExcelPath path = new SetExcelPath();
		ReadExcelDataUsingUtilClass read_excel_data = new ReadExcelDataUsingUtilClass(path.return_excel_path());
		GetRowNumberOnTC testcase_row_number = new GetRowNumberOnTC();
		 
		
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)       
					.withTimeout(10000, TimeUnit.MILLISECONDS)    
					.pollingEvery(5, TimeUnit.MILLISECONDS);
		  
		System.out.println(" Waiting to Load -------------Service Profile For New Customer page -------------- ");
		
		for(int i=0 ;i<40;i++)
		{						
			try {
				String pagename = driver.findElement(By.xpath("//*[@class ='headerBarblock']")).getText();
				System.out.println(pagename);
				
				
				final WebElement beginorder =driver.findElement(Locators.Beginorder);
				
				
				if(beginorder.isDisplayed())
				{
					i=40;
									
					String pageTitle = driver.getTitle();
					System.out.println("Title of the current page is : " + pageTitle);
					
					services_Excel(driver, TestCasename, read_excel_data, testcase_row_number);
					
					test.info("LOB Selection done", MediaEntityBuilder.createScreenCaptureFromPath(System.getProperty("user.dir")+"\\Report\\"
					+TestCasename+"\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCasename)).build());
					
					
					JavascriptExecutor js = (JavascriptExecutor) driver;
					
					js.executeScript("arguments[0].click();", driver.findElement(Locators.ClickLater));

					test.info("Credit Check Cancel", MediaEntityBuilder.createScreenCaptureFromPath(System.getProperty("user.dir")+"\\Report\\"
							+TestCasename+"\\Screenshots\\"+
							report_generator.take_ScreenShot(driver, TestCasename)).build());
					
					
					js.executeScript("arguments[0].click();", beginorder );
					
						System.out.println("---------------Waiting to Load Build My Bundle Page --------------------");
				}
				
				else
				{
					wait.until(new Function<WebDriver, WebElement>() {       
						public WebElement apply(WebDriver driver) { 
						return beginorder;  
						 }  
						});  
				}
				status = true;
			}
			catch(Exception e)
			{
				status =false;
			}
		}
				
		return status;
		
	}
	
	

	public void services_Excel(WebDriver driver, String TestCaseName,ReadExcelDataUsingUtilClass read_excel_data, GetRowNumberOnTC testcase_row_number) throws Exception
	{

	 hsiaService(driver, TestCaseName, read_excel_data,testcase_row_number );
	 WirelessService(driver, TestCaseName, read_excel_data,testcase_row_number );
	 DTVService(driver, TestCaseName, read_excel_data,testcase_row_number );
	 VOIPService(driver, TestCaseName, read_excel_data,testcase_row_number );
	}
	
	
	public void hsiaService(WebDriver driver, String TestCaseName, ReadExcelDataUsingUtilClass read_excel_data, GetRowNumberOnTC testcase_row_number) throws Exception
	{
		
		
		hsia =read_excel_data.getCellData("TestCase", "AT&T Internet",testcase_row_number.getRownumber(TestCaseName, "TestCase") );
		
	if(hsia.equals("Y"))
		{
		boolean status =driver.findElement(Locators.hsia).isEnabled();
		if(status==true)
		{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", driver.findElement(Locators.hsia));
		System.out.println(" HSIA Selected ");
	
		}
		else 
		{
			System.out.println("HSIA Service checkbox is disabled");
		}
	}
	else
	 {
		 System.out.println(" HSIA is not selected " );
	 }
	
}
	
	public void WirelessService(WebDriver driver, String TestCaseName, ReadExcelDataUsingUtilClass read_excel_data, GetRowNumberOnTC testcase_row_number) throws Exception
	{
		wls =read_excel_data.getCellData("TestCase", "WLS",testcase_row_number.getRownumber(TestCaseName, "TestCase") );
	if(wls.equals("Y"))
		{
		boolean status =driver.findElement(Locators.wireless).isEnabled();
		if(status==true)
		{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", driver.findElement(Locators.wireless));
		System.out.println(" Wiresless Selected ");
			
		}
		else 
		{
			System.out.println("Wireless Service checkbox is disabled");
		}
	}
	else
	 {
		 System.out.println(" Wireless is not selected " );
	 }
	
}
	
	public void DTVService(WebDriver driver, String TestCaseName, ReadExcelDataUsingUtilClass read_excel_data, GetRowNumberOnTC testcase_row_number) throws Exception
	{
		
		dtv =read_excel_data.getCellData("TestCase", "DIRECTV",testcase_row_number.getRownumber(TestCaseName, "TestCase") );
	if(dtv.equals("Y"))
		{
		boolean status =driver.findElement(Locators.dtv).isEnabled();
		if(status==true)
		{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", driver.findElement(Locators.dtv));
		System.out.println(" DTV Selected ");
		
		}
		else 
		{
			System.out.println("DTV Service checkbox is disabled");
		}
	}
	else
	 {
		 System.out.println(" DTV is not selected " );
	 }
	
}
	
	
	public void VOIPService(WebDriver driver, String TestCaseName, ReadExcelDataUsingUtilClass read_excel_data, GetRowNumberOnTC testcase_row_number) throws Exception
	{
		
		voip =read_excel_data.getCellData("TestCase", "AT&T Phone",testcase_row_number.getRownumber(TestCaseName, "TestCase") );
	if(voip.equals("Y"))
		{
		boolean status =driver.findElement(Locators.voip).isEnabled();
		if(status==true)
		{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", driver.findElement(Locators.voip));
		System.out.println(" VOIP Selected ");
		
		}
		else 
		{
			System.out.println("VOIP Service checkbox is disabled");
		}
	}
	else
	 {
		 System.out.println(" VOIP is not selected " );
	 }
	
}
	
	
}


