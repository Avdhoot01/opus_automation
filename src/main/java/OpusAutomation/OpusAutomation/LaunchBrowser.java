package OpusAutomation.OpusAutomation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import basicFactor.GetRowNumberOnTC;
import basicFactor.ReadExcelDataUsingUtilClass;
import basicFactor.SetExcelPath;
import lib.Locators;

public class LaunchBrowser {

	String Env_Type;
	SetExcelPath path = new SetExcelPath();
	ReadExcelDataUsingUtilClass read_excel_data = new ReadExcelDataUsingUtilClass(path.return_excel_path());
	GetRowNumberOnTC row_number = new GetRowNumberOnTC();
	static ExtentTest test;
	static ExtentReports report;
	public LaunchBrowser(String Env_Type) throws Exception {
		this.Env_Type = Env_Type;
	}

//	public static void teadDown(WebDriver driver, ExtentReports report) 
	public static void teadDown(ExtentReports report) 
	{
		report.flush();
//		driver.close();
//		driver.quit();
	}
	
	
	public WebDriver setUp(WebDriver driver, ExtentTest test, ExtentReports report) throws Exception {

		try {
			System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+".\\driver\\IEDriverServer_64.exe");


			InternetExplorerOptions IEoptions=new InternetExplorerOptions();
			IEoptions.ignoreZoomSettings();
			IEoptions.introduceFlakinessByIgnoringSecurityDomains();
			IEoptions.destructivelyEnsureCleanSession();
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			cap.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);
			cap.setCapability(InternetExplorerDriver.IE_SWITCHES, "-private");
	
			driver=new InternetExplorerDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


			String url="";
			url = read_excel_data.getCellData("EnvDetails", "Url", row_number.getRownumber(Env_Type, "EnvDetails"));
			driver.navigate().to(url);
			driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
			test.pass("Nevigate to OPUS URL");
		}
		catch(Exception e) {
			test.fail("Browser launch failed\n"+e.getMessage());
			System.out.println(e.getMessage());
//			teadDown(driver, report);
			teadDown(report);
		}
		return driver;
	}

	public void StoreSelect(WebDriver driver, ExtentTest test, ExtentReports report) {

		try {
			
//			if (driver.getTitle().equals("OPUS Login")) {
				WebDriverWait wait = new WebDriverWait(driver,200);
				String storeSelect;
				storeSelect = read_excel_data.getCellData("EnvDetails", "Store", row_number.getRownumber(Env_Type, "EnvDetails"));
				System.out.print(storeSelect);
				WebElement storeid=wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.storeid));
				Select store = new Select(storeid);
				Thread.sleep(2000);
				store.selectByVisibleText(storeSelect); 

				System.out.println("store selected");
				test.pass("Store Selected: "+storeSelect);

				WebDriverWait wait1 = new WebDriverWait(driver,200);
				WebElement login = wait1.until(ExpectedConditions.visibilityOfElementLocated(Locators.clickLogin));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].click();", login);	
				test.pass("Login button clicked");
//			}
//			else {
//				test.fail("Dealer Code page loaded");
//			}
			
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			test.fail(e.getMessage());
		}
	}


	public void globalLogon(WebDriver driver,  ExtentTest test, ExtentReports report) throws Exception
	{	
		
		try {
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			String userid =""; 
			userid = read_excel_data.getCellData("EnvDetails", "userid", row_number.getRownumber(Env_Type, "EnvDetails" ));

			String password ="";
			password = read_excel_data.getCellData("EnvDetails", "password", row_number.getRownumber(Env_Type, "EnvDetails" ));

			System.out.println("userid is "+userid+ ","+"password is: "+password);

			WebDriverWait wait = new WebDriverWait(driver,10);

			wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.Userid));
			driver.findElement(Locators.Userid).sendKeys(userid);
			test.info("UserID set successfully: "+userid);
			driver.findElement(Locators.pwd).sendKeys(password);
			test.info("Password set successfully: "+password);
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.clickSubmit));
			WebElement btnSubmit =driver.findElement(Locators.clickSubmit);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", btnSubmit);

			test.info("Submit button clicked");
			Thread.sleep(4000);
//			WebElement successOK=wait.until(ExpectedConditions.visibilityOfElementLocated(Locators.clickOK));
			WebElement successOK = driver.findElement(Locators.clickOK);
			JavascriptExecutor js1 = (JavascriptExecutor) driver;
			js1.executeScript("arguments[0].click();", successOK);
			test.pass("Login Successfull");
		}
		catch(NoSuchElementException e) {
			System.out.println(e.getMessage());
			test.fail("Login failed\n"+e.getMessage());
		}
	}
}
