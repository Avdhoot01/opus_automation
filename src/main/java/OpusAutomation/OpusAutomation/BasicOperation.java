package OpusAutomation.OpusAutomation;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

import lib.Locators;
import basicFactor.ExtentReportGenerator;
import basicFactor.GetRowNumberOnTC;
import basicFactor.ReadExcelDataUsingUtilClass;
import basicFactor.SetExcelPath;

public class BasicOperation {
	String Testcase;
	SetExcelPath path = new SetExcelPath();
	ReadExcelDataUsingUtilClass read_excel_data = new ReadExcelDataUsingUtilClass(path.return_excel_path());
	GetRowNumberOnTC testcase_row_number = new GetRowNumberOnTC();
	ExtentReports report;
	ExtentTest  test;
	ExtentReportGenerator report_generator = new ExtentReportGenerator();
	boolean status;

	public String trim_digit(String digit) {
		if (digit.contains(".0")) {
			digit = digit.substring(0, digit.length()-2);
		}
		return digit;
	}

	public BasicOperation(String TestCase) throws Exception {
		Testcase=TestCase;
	}

	public void validateQuote(WebDriver driver) throws Exception {


		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(90000, TimeUnit.MILLISECONDS) 			
				.pollingEvery(1500, TimeUnit.MILLISECONDS) 			
				.ignoring(NoSuchElementException.class);

		//		Screenshot.captureScreenshot(driver, "validate order");

		WebElement clickvalidateorder = wait.until(new Function<WebDriver, WebElement>(){

			public WebElement apply(WebDriver driver ) {
				return driver.findElement(By.xpath("//*[@id='validateQuote']"));
			}
		});


		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", clickvalidateorder);
		js.executeScript("arguments[0].click();", clickvalidateorder);
		System.out.println("Validate quote submitted");
		
	}

	public void check_ValidationMessage(WebDriver driver, ExtentTest test) throws Exception {

		for (int i =0; i <5; i++) {
			try {
				System.out.println("Retring--> "+i);
				WebElement msg = driver.findElement(By.xpath("//div[.=\"Please review the items below and make any necessary changes.\"]"));
				if (msg.isDisplayed()) {
					summery_popUp_OK_click(driver, test);
					break;
				}

			}
			catch (Exception e) {
				Thread.sleep(30000);
			}
		}	
	}

	public void summery_popUp_OK_click(WebDriver driver, ExtentTest test) throws Exception{

		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(90000, TimeUnit.MILLISECONDS) 			
				.pollingEvery(25000, TimeUnit.MILLISECONDS) 			
				.ignoring(NoSuchElementException.class);

		WebElement Ok =wait.until(new Function<WebDriver, WebElement>(){

			public WebElement apply(WebDriver driver ) {
				return driver.findElement(By.xpath("//*[@id=\"closeOverlay\"]"));
			}
		});
		//		Screenshot.captureScreenshot(driver, "OK submitted");	

		test.info("OK button clicked",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
				report_generator.take_ScreenShot(driver, Testcase)).build() );

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", Ok);
		System.out.println("OK submitted");


	}


	public void click_continue_to_next_forward(WebDriver driver, ExtentTest test) throws Exception{


		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(90000, TimeUnit.SECONDS) 			
				.pollingEvery(15, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);

		WebElement contiue =wait.until(new Function<WebDriver, WebElement>(){

			public WebElement apply(WebDriver driver ) {
				return driver.findElement(By.xpath("//input[@name=\"continueOrderBtn\" and @id=\"forwardToNextPage\" and @type=\"button\"]"));
			}
		});



		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", contiue);
		js.executeScript("arguments[0].click();", contiue);
		System.out.println("Continue submitted");

	}


	public boolean autoPay_PaperLessBillcheckBoxHandle(WebDriver driver, ExtentTest test) throws Exception{
		String AutoPay_Check= read_excel_data.getCellData("TestCase", "AutoPay",testcase_row_number.getRownumber(Testcase, "TestCase") );
		String Paperless_Check = read_excel_data.getCellData("TestCase", "PaperlessBill",testcase_row_number.getRownumber(Testcase, "TestCase") );


		try {
			Thread.sleep(2000);
			//			Screenshot.captureScreenshot(driver, "Before Autopay handle");

			if (! AutoPay_Check.equals("Y")) {

				WebElement autoPay_checkBox = driver.findElement(By.xpath("//input[@name=\"autoBillPayOption\"]"));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView();", autoPay_checkBox);
				js.executeScript("arguments[0].click();", autoPay_checkBox);
				System.out.println("Autopay uncheked");
				test.info("Autopay uncheked");

				//				Screenshot.captureScreenshot(driver, "After Autopay handle");
			}

			if (! Paperless_Check.equals("Y")) {

				WebElement paperless_checkBox = driver.findElement(By.xpath("//input[@name=\"paperLessBillOption\"]"));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView();", paperless_checkBox);
				js.executeScript("arguments[0].click();", paperless_checkBox);
				System.out.println("Paperless uncheked");
				//				Screenshot.captureScreenshot(driver, "After papereless checkbox handle");
				test.info("Paperless uncheked");
			}
			status = true;
		}

		catch (Exception e) 
		{
			status = false;
			test.fail(e);
		}
		return status;
	}




}
