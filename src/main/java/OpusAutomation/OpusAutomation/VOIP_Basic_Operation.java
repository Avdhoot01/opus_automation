package OpusAutomation.OpusAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import basicFactor.ExtentReportGenerator;
import basicFactor.GetRowNumberOnTC;
import basicFactor.ReadExcelDataUsingUtilClass;
import basicFactor.SetExcelPath;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

public class VOIP_Basic_Operation {
	String Testcase;	
	boolean status;

	SetExcelPath path = new SetExcelPath();
	ReadExcelDataUsingUtilClass read_excel_data = new ReadExcelDataUsingUtilClass(path.return_excel_path());
	GetRowNumberOnTC testcase_row_number = new GetRowNumberOnTC();
	ExtentReportGenerator report_generator = new ExtentReportGenerator();

	public VOIP_Basic_Operation(String TestCase) throws Exception {
		Testcase=TestCase;
	}


	public void switch_to_VOIP(WebDriver driver, ExtentTest test) throws Exception{
		
		WebDriverWait wait = new WebDriverWait(driver,250);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='phoneOffer']")));			
		
		WebElement clickVoip=driver.findElement(By.xpath("//*[@id='phoneOffer']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", clickVoip);
		
		test.info("Switch to VOIP", MediaEntityBuilder.createScreenCaptureFromPath(".\\"
				+"\\Screenshots\\"+
				report_generator.take_ScreenShot(driver, Testcase)).build());
		
	}
	
	public boolean Select_VOIP_Package(WebDriver driver, ExtentTest test) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,250);
		
		
		String VOIPPlan = read_excel_data.getCellData("TestCase", "Phone Price Plan",testcase_row_number.getRownumber(Testcase,"TestCase" ) );
		String all_included = read_excel_data.getCellData("TestCase", "Phone Price Plan Included",testcase_row_number.getRownumber(Testcase, "TestCase") );
		
		try {
			System.out.println("Waiting....");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(), \"Phone Unlimited N. America\")]//..//*[.=\"Included!\"]//..//input[@id='productSelectedOfferForVoip']")));
			WebElement VOIP_Package = driver.findElement(By.xpath("//div[contains(text(), \"Phone Unlimited N. America\")]//..//*[.=\"Included!\"]//..//input[@id='productSelectedOfferForVoip']"));
			
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", VOIP_Package);
			
			js.executeScript("arguments[0].click();", VOIP_Package);
			test.info("Price plan Selected", MediaEntityBuilder.createScreenCaptureFromPath(".\\"
					+"\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
			status = true;
		}
		catch(Exception e) {
			test.fail("e", MediaEntityBuilder.createScreenCaptureFromPath(".\\"
					+"\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, Testcase)).build());
			status = false;
		}
		
		return status;
	}

}