package lib;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class Locators 
{
	


	WebDriver driver;
	
	//** ========================================= Launch and Login to OPUS ========================================**//
	
	public static By storeid = By.xpath("//*[@name='homeURL']");
	public static By clickLogin = By.xpath("//*[@id='btnLogin']");
	public static By Userid = By.xpath("//*[@id='GloATTUID']");
	public static By pwd = By.xpath("//*[@id='GloPassword']");
	public static By clickSubmit = By.xpath("//*[@id='GloPasswordSubmit']");
	public static By clickOK = By.xpath("//*[@name='successOK']");
	
	// *-------------------------Home Page: Assist Customer Now--//OPUSCreateCustOPS ----------------------*	
	
	public static By processLater = By.xpath("//*[.=' Process Later ']");
	
	public static By newCust = By.xpath("//*[@id='new-customer-gray']");
	public static By address1 =By.xpath("//*[@name='standardAddress.address1']");
	public static By addressCity =By.xpath("//*[@name='standardAddress.city']");
	public static By zip =By.xpath("//*[@name='standardZip']");
	
	
	public static By checkAvailabilty = By.xpath("//*[@id='letsgo']");
	
	// *-------------------------Service _Profile Page----------------------*	
	public static By Beginorder = By.xpath("//*[@id='orderNowBtn']");
	public static By hsia = By.xpath("//*[@id='uvInternetAvblCb']");
	public static By dtv = By.xpath("//*[@id='dtvAvblCb']");
	public static By voip = By.xpath("//*[@id='uvVoipAvblCB']");
	public static By ClickLater = By.xpath("//*[text()='Later']");
	public static By wireless = By.xpath("//input[@id='wirelessAvblCb']");
	
	// *-------------------------Build My Own Bundle Page ----------------------*	
		
	public static By csi_fed_convid = By.xpath("//*[contains(text(),'opus~CNG-CSI~')]");
	public static By validateQuote = By.xpath("//*[@id='validateQuote']");
	public static By Closepopup =By.xpath("//*[@id='closeOverlay']");

	public static By contOrder = By.xpath("//*[@id='forwardToNextPage']");
	public static By monthlycharges = By.xpath("//*[text()='Monthly Charges']");
	public static By ccwebPortal = By.xpath("//*[text()='CCWebPortal']");
	
	
	//public static By cancel = By.xpath("//*[text()='Cancel']");

	// *-------------------------Create Account Page  ----------------------*//
	
	public static By fname = By.xpath("//*[@id='customerNameFirst']");
	public static By lname = By.xpath("//*[@id='customerNameLast']");
	public static By cdobmonth = By.xpath("//*[@id='dobMonth']");
	public static By cdobday= By.xpath("//*[@id='dobDay']");
	public static By cdobyear= By.xpath("//*[@id='dobYear']");
	public static By cSSN= By.xpath("//*[@id='omCustomerSSN']");
	public static By confirmcSSN= By.xpath("//*[@id='omConfirmSSN']");
	
	public static By sbutton = By.xpath("//*[@id='buttonSubmitCustInfo']");
	
	public static By license_state = By.xpath("//*[@id='customerdlstate']");
	public static By license_number = By.xpath("//*[@id='customerDlNumber']");
	public static By licensemonth = By.xpath("//*[@id='expMonth']");
	public static By licenseday= By.xpath("//*[@id='expDay']");
	public static By licenseyear= By.xpath("//*[@id='expYear']");
	
	public static By sbutton2=By.xpath("//*[@id='buttonSubmitCustInfo']");
	
	// *-------------------------Create Account Page2 -Security/Contact/Billing Address information  ----------------------*//

	public static By securityPasscode= By.xpath("//*[@id='securityPin']");
	public static By securityAnswer= By.xpath("//*[@id='securityAnswer']");
	public static By primaryPhone= By.xpath("//*[@id='primaryPhoneNumber.phoneNumber']");
	public static By primaryPhoneType = By.xpath("//*[@id='unifiedCustomer.primaryPhoneType']");
	public static By contactPreference= By.xpath("//*[@id='unifiedCustomer.primaryPhoneContactPreference']");

	public static By secPhone =  By.xpath("//*[@id='secondaryPhoneNumber.phoneNumber']");
	public static By alternativePhoneType =  By.xpath("//*[@id='unifiedCustomer.secondaryPhoneType']");
	public static By secContactPreference =  By.xpath("//*[@id='unifiedCustomer.secondaryPhoneType']");
	
	public static By primaryEmailCcheckbox= By.xpath("//*[@id='noEmailCB']");
	
	public static By prefContactMethod= By.xpath("//*[@id='preferredContactMode']");
	public static By prefContactTime= By.xpath("//*[@id='preferredContactTime']");
	
	public static By billingAddressCheck= By.xpath("//*[@id='billAddrchkbx']");
	public static By nextButton= By.xpath("//*[@id='btnNxt']");

	// *-------------------------Create Account Page -Credit Result  ----------------------*//

	public static By creditReviewChkbox= By.xpath("//*[@id='creditReviewCheck']");
	
	public static By continueButton = By.xpath("//*[@id='btnReviewFeatures']");
	
	// *-------------------------Negotiate Due Date Page  ----------------------*//
	
	// public static By installcheckList= By.xpath("//*[text()='Controlling the Controllable Installation Checklist']");
	 
	 public static By installcheckList= By.linkText("Controlling the Controllable Installation Checklist");
	 public static By ackContinueButton = By.xpath("//*[@id='continueButton']");

	
	public static final By NDDNext = By.xpath("//*[@id='buttonNext']");
	
	// *-------------------------Review and submit Page  ----------------------*//

	public static final By reviewNext = By.xpath("//*[@id='buttonNext']");
	
	//public static final By autoPayYes = By.xpath("//*[@id='optionalEnrollYes']");
	//public static final By autoPayNo = By.xpath("//*[@id='optionalEnrollNo']");
	
	//public static final By optionalAutoPayYes = By.xpath("//*[@id='optionalAutopPayYes']");
	//public static final By optionalAutoPayNO = By.xpath("//*[@id='optionalAutopPayNo']");
	
	//public static final By creditDebitCard = By.xpath("//*[@id='credit']");
	public static final By cardNumber = By.xpath("//*[@id='cardNumber']");
	public static final By cardExpMonth = By.xpath("//*[@id='expMonth']");
	public static final By cardExpYear = By.xpath("//*[@id='expYear']");
	
	public static final By nameOnCard = By.xpath("//*[@id='nameOnCard']");
	public static final By zipCode = By.xpath("//*[@id='zipCode']");
	public static final By cvvNumber = By.xpath("//*[@id='cvvNbr']");
	
	
	public static final By SavingAcc = By.xpath("//*[@id='checking']");
	
	
	
	public static final By clickEnroll = By.xpath("//*[@id='EnrollAutopayButton']");
	

	public static final By ebillEnrollQuestNO = By.xpath("//*[@id='paperlessEnrollNo']");
	
	
	// *-------------------------Review Order  Page  ----------------------*//
	
	public static final By expandDTV = By.xpath("//*[@id='dtvMrcButton']");
	public static final By disclosurechkBx = By.xpath("//*[@id='dtvETFDisclosureChkBx']");


	
	public static By reviewNextorder = By.xpath("//*[@id='reviewOrderbtnNext']");
	
	// *-------------------------Review Order Continue Page  ----------------------*//
	

	public static By rtpDisclosureChkBX  = By.xpath("//*[@id='rtpDisclouserCheck']");
	
	public static By eSignature  = By.xpath("//*[@id='emailSigCapButtonId']");
	public static By signit  = By.xpath("//canvas[@id='blank']");
	

	public static By iAccept = By.xpath("//*[@id='saveSig']");

	
	
	// *-------------------------DTV Promotional Offers Disclosure Page  ----------------------*//
 
	public static final By DTVPONext = By.xpath("//*[@id='btnNxtContinueDesktop']");
	
	// *-------------------------Unified Credit Policy Requirement Page  ----------------------*//
	 

		public static final By DTVCPNext = By.xpath("//*[@id='buttonNext']");
		

		// *-------------------------Unified Credit Policy Requirement Page  ----------------------*//
		 	public static final By SOdisclosurechkBx = By.xpath("//*[@id='disclosuresReviewCheck']");
		 	
		 	public static final By SOreviewdisclosureLink = By.xpath("//*[@id='reviewdisclosurelink']");

			
		 	
		 	public static By submitButton = By.xpath("//*[@id='btnSubmit']");
		 	
		 // *-------------------------Unified Credit Policy Requirement Page  ----------------------*//
		 	public static By taxSubmitButton = By.xpath("//*[@id='btnSubmit']");

		 // *-------------------------Customer Checkout  Page  ----------------------*//
		 	public static By creditCardSelect = By.xpath("//*[@id='bold_credit_card']");
		 	public static By streetNumber = By.xpath("//*[@name='streetNumber']");
		 	public static final By BzipCode = By.xpath("//*[@name='zipCode']");

			
		 	public static By nameonCard = By.xpath("//*[@name='cardHolderName']");
		 	public static By cradnumber = By.xpath("//*[@id='cardNumber']");
		 	public static By expmonth = By.xpath("//*[@id='expMonth']");
		 	public static By expYear = By.xpath("//*[@id='expYear']");
		 	public static By cardNumberLast4Digit = By.xpath("//*[@id='cardNumberLast4Digite']");
		 	
		 	public static By submitbutton = By.xpath("//*[@id='opusmobileccSubmit']");
		 	
		 	 // *-------------------------Customer Checkout_Customer sign Page  ----------------------*//

		 	
		 	public static By SkipButton = By.xpath("//*[@id='btnSkip']");
		 	
 // *-------------------------Receipt Preference Page  ----------------------*//

		 	
		 	public static By Custemail = By.xpath("//*[@name='emailSelected']");
		 	
		 	public static By nextClick = By.xpath("//*[@id='btncreate']");
		 	
		 	 // *-------------------------Order Confirmation Page  ----------------------*//

		 	public static final By Ban= By.xpath("//*[@class='selectWidth25 floatLeft blueText']/div");
		 	public static By BtnOK = By.xpath("//*[@id='btnOkk']");
	


		
		 	

			
}
