package basicFactor;
import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class GetRowNumberOnTC {
	
	public int getRownumber(String testcase_name, String sheetname) {
		XSSFWorkbook wb;
		XSSFSheet TestCase;
		int rownumber= 0;
		SetExcelPath path = new SetExcelPath();
		
		try {
			File dataFile = new File(path.return_excel_path());
			FileInputStream fis = new FileInputStream(dataFile); 
			wb = new XSSFWorkbook(fis);
			TestCase=wb.getSheet(sheetname);

			Iterator<Row> rowIterator = TestCase.iterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				Iterator<Cell> cellIterator = row.cellIterator();
				
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_NUMERIC:
						break;
					case Cell.CELL_TYPE_STRING:
						if (cell.getStringCellValue().equals(testcase_name)){
							rownumber=row.getRowNum()+1;
						}
					}
					
				}
			}
			
		}
	catch (Exception e) {
		e.printStackTrace();
	}
		return rownumber;
	}
}