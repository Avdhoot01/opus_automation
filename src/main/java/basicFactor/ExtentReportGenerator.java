package basicFactor;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReportGenerator {
	static ExtentHtmlReporter htmlreporter;
	static ExtentReports extent;
	static ExtentTest test;
	String TestCase;
	public List<Object> create_Report(String TestCase) throws IOException {
		this.TestCase = TestCase;
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		File theDir = new File(System.getProperty("user.dir")+"\\Report\\"+TestCase);
		theDir.mkdirs();
		FileUtils.cleanDirectory(new File(System.getProperty("user.dir")+"\\Report\\"+TestCase));
		
		//		htmlreporter  = new ExtentSparkReporter("HTML_Report_"+timeStamp+".html");
		htmlreporter  = new ExtentHtmlReporter(System.getProperty("user.dir")+"\\Report\\"+TestCase+"\\"+TestCase+"_"+timeStamp+".html");
		extent = new ExtentReports();

		extent.attachReporter(htmlreporter);

		test=extent.createTest(TestCase);

		return Arrays.asList(test, extent);
	}


	public String take_ScreenShot(WebDriver driver, String TestCase) throws IOException {
		
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		
		File theDir = new File(System.getProperty("user.dir")+"\\Report\\"+TestCase+"\\Screenshots");
		theDir.mkdirs();
		
		
		File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage img = ImageIO.read(screen);
		ImageIO.write(img, "png", new File(System.getProperty("user.dir")+"\\Report\\"+TestCase+"\\Screenshots\\" +timeStamp+"_Test.png"));
		
		return timeStamp+"_Test.png";
	}

}
