package OpusAutomation.OpusAutomation;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

import basicFactor.ExtentReportGenerator;

public class PR_WLS_Standalone {
	WebDriver driver = null;
	static ExtentTest test;
	static ExtentReports report;
	static LaunchBrowser setup;
	boolean status = true;
	ExtentReportGenerator report_generator = new ExtentReportGenerator();
	static String TestCase ="PR Standalone WLS";
	
	WirelessOperations wls_operation;
	
	BasicOperation boperaion ;

	@BeforeTest
	public void setUp() throws Exception{
		List <Object> list_call= report_generator.create_Report(TestCase);

		test =(ExtentTest) list_call.get(0);
		report =(ExtentReports) list_call.get(1);

		setup = new LaunchBrowser("TEST_QC5");
		driver=setup.setUp(driver, test, report);

		System.out.println("Set Up");
	}

	@Test(priority = 1)
	public void test_StoreSelect_activity_1(){
		setup.StoreSelect(driver, test, report);
	}

	@Test(priority = 2)
	public void test_GlobanLogon_activity_2() throws Exception{
		setup.globalLogon(driver, test, report);;
	}

	@Test(priority = 3)
	public void test_AddressValidation_activity_3() throws Exception{
		CreateContactAddressPage create_contact = new CreateContactAddressPage();
		status =create_contact.new_customer_click(driver,TestCase, test, report);

		if (status) {
			test.pass("Address Validation Pass");
		}
		else {
			test.fail("Address validation Failed");
//			throw new RuntimeException("Address validation Failed");
			setup.teadDown( report);
//			System.exit(0);
			
		}
	}
	
	@Test(priority = 4)
	public void test_LOBSelect_Activity_4( ) throws Exception{

		ServiceProfileForNewCust selectLOB = new ServiceProfileForNewCust();
		status =selectLOB.serviceAvailability(driver, TestCase, test, report);


		if (status) {
			test.pass("LOB selected");
		}
		else {
			test.fail("LOB not selected");
			
		}

	}
	
	
	
	@Test(priority = 5)
	public void test_PhonePlusButton_Activity() throws Exception{
		wls_operation = new WirelessOperations(TestCase);
		status = wls_operation.Click_Phone_Plus_Button(driver, test);
		if (status) {
			test.pass("Phone plus Button clicked",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else {
			test.fail("Phone plus button not visible",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		
	}

	@Test(priority = 6)
	public void test_EnterCustomerDetailsActivity() throws Exception{
		wls_operation = new WirelessOperations(TestCase);
		status =wls_operation.enter_customer_details(driver, test);
		if (status) {
			test.pass("Customer Details add",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else {
			test.fail("Customer deatils enter failed",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		
}
	
		 @Test(priority = 7)
			public void test_DeviceSelectionActivity() throws Exception{
				wls_operation = new WirelessOperations(TestCase);
				status =wls_operation.Device_Selection(driver, test);
				if (status) {
					test.pass("Device selection done",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
							report_generator.take_ScreenShot(driver, TestCase)).build() );
				}
				else {
					test.fail("Not able to add device",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
							report_generator.take_ScreenShot(driver, TestCase)).build() );
				}
		 }
		 

		 @Test(priority = 8)
			public void test_PricePlanSelectionActivity() throws Exception{
				wls_operation = new WirelessOperations(TestCase);
				status =wls_operation.SelectPricePlan(driver, test);
				
				if (status) {
					test.pass("Price plan selected",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
							report_generator.take_ScreenShot(driver, TestCase)).build() );
				}
				else {
					test.fail("Price plan select failed",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
							report_generator.take_ScreenShot(driver, TestCase)).build() );
				}
		 }
		 
		 
		 @Test(priority = 9)
			public void test_AutoPay_BillHandle_Activity_11() throws Exception{
				
				boperaion =new BasicOperation(TestCase);
				status =boperaion.autoPay_PaperLessBillcheckBoxHandle(driver, test);
				
				if (status) {
					test.pass("AutoPay_BillHandle_Activity Succesfull",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
							report_generator.take_ScreenShot(driver, TestCase)).build() );
				}
				else {
					test.fail("AutoPay_BillHandle_Activity fail",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
							report_generator.take_ScreenShot(driver, TestCase)).build() );
				}
			}
			
			
			@Test(priority = 10)
			public void test_ValidateQuote_Activity_12() throws Exception
			{
				boperaion =new BasicOperation(TestCase);
				try {
					boperaion.validateQuote(driver);
					test.pass("Validate Quote done", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
							report_generator.take_ScreenShot(driver, TestCase)).build() );
				}
				catch(Exception e) {
					test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
							report_generator.take_ScreenShot(driver, TestCase)).build());
				}
			}
			
			
			@Test(priority = 11)
			public void test_ClosePopUpButton_Activity_13() throws Exception{
				boperaion =new BasicOperation(TestCase);
				try {
					boperaion.check_ValidationMessage(driver, test);
					test.pass("Popup Closed", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
							report_generator.take_ScreenShot(driver, TestCase)).build() );
				}
				catch(Exception e) {
					test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
							report_generator.take_ScreenShot(driver, TestCase)).build());
				}
				
			}
			

			@AfterTest
			public void teadDown() throws Exception {
				report_generator.take_ScreenShot(driver, TestCase);
//				setup.teadDown(driver, report);
				setup.teadDown( report);
			}
			
	
}
