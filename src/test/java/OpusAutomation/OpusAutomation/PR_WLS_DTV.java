package OpusAutomation.OpusAutomation;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

import basicFactor.ExtentReportGenerator;

public class PR_WLS_DTV {
	WebDriver driver = null;
	static ExtentTest test;
	static ExtentReports report;
	static LaunchBrowser setup;
	boolean status = true;
	ExtentReportGenerator report_generator = new ExtentReportGenerator();
	static String TestCase ="PR WLS_DTV";

	DTV_Basics_Operations dtv_operation;
	WirelessOperations wls_operation;
	VOIP_Basic_Operation voip_operation;
	BasicOperation boperaion ;
	CreateAccount_Personal_Information info;

	@BeforeTest
	public void setUp() throws Exception{
		List <Object> list_call= report_generator.create_Report(TestCase);

		test =(ExtentTest) list_call.get(0);
		report =(ExtentReports) list_call.get(1);

		setup = new LaunchBrowser("TEST_QC5");
		driver=setup.setUp(driver, test, report);

		System.out.println("Set Up");
	}



	@Test(priority = 1)
	public void test_StoreSelect_activity_1(){
		setup.StoreSelect(driver, test, report);
	}

	@Test(priority = 2)
	public void test_GlobanLogon_activity_2() throws Exception{
		test.info("Global Logon", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
				report_generator.take_ScreenShot(driver, TestCase)).build() );
		setup.globalLogon(driver, test, report);;
	}

	@Test(priority = 3)
	public void test_AddressValidation_activity_3() throws Exception{
		CreateContactAddressPage create_contact = new CreateContactAddressPage();
		status =create_contact.new_customer_click(driver,TestCase, test, report);

		if (status) {
			test.pass("Address Validation Pass");
		}
		else {
			test.fail("Address validation Failed");
			setup.teadDown( report);
			//			System.exit(0);

		}
	}

	@Test(priority = 4)
	public void test_LOBSelect_Activity_4( ) throws Exception{

		ServiceProfileForNewCust selectLOB = new ServiceProfileForNewCust();
		status =selectLOB.serviceAvailability(driver, TestCase, test, report);


		if (status) {
			test.pass("LOB selected");
		}
		else {
			test.fail("LOB not selected");

		}

	}

	@Test(priority = 5)
	public void test_PhonePlusButton_Activity() throws Exception{
		wls_operation = new WirelessOperations(TestCase);
		status = wls_operation.Click_Phone_Plus_Button(driver, test);
		if (status) {
			test.pass("Phone plus Button clicked",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else {
			test.fail("Phone plus button not visible",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}

	}

	@Test(priority = 6)
	public void test_EnterCustomerDetailsActivity() throws Exception{
		wls_operation = new WirelessOperations(TestCase);
		status =wls_operation.enter_customer_details(driver, test);
		if (status) {
			test.pass("Customer Details add",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else {
			test.fail("Customer deatils enter failed",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}

	}

	@Test(priority = 7)
	public void test_DeviceSelectionActivity() throws Exception{
		wls_operation = new WirelessOperations(TestCase);
		status =wls_operation.Device_Selection(driver, test);
		if (status) {
			test.pass("Device selection done",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else {
			test.fail("Not able to add device",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
	}


	@Test(priority = 8)
	public void test_PricePlanSelectionActivity() throws Exception{
		wls_operation = new WirelessOperations(TestCase);
		status =wls_operation.SelectPricePlan(driver, test);

		if (status) {
			test.pass("Price plan selected",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else {
			test.fail("Price plan select failed",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
	}

	// ********************** DTV Validation ****************************

	@Test(priority = 9)

	public void test_DTV_BasePackageSelect_Activity_5()  throws Exception{

		dtv_operation = new DTV_Basics_Operations(TestCase);
		dtv_operation.Switch_TV(driver);
		status =dtv_operation.selectDTVPackage(driver, test, report);
		if (status) {
			test.pass("DTV Base Package selected",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else {
			test.fail("DTV Base Package not selected",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}

	}

	@Test(priority = 10)
	public void test_check_to_add_promotion_Activity_6() throws Exception{
		dtv_operation.check_to_add_promotion(driver, "Y", test);
	}

	@Test(priority =11)

	public void test_TVAdd_Activity_7()  throws Exception{

		status =dtv_operation.number_of_tv_add(driver, test, report);


		if (status) {
			test.pass("TV added Succesfully",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else {
			test.fail("Not able to add TV",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );

		}

	}


	@Test(priority = 12)

	public void test_ReceiverAdd_Activity_8()  throws Exception{

		status =dtv_operation.add_reciver(driver, test, report);


		if (status) {
			test.pass("Reciever added Succesfully",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else {
			test.fail("Not able to add receiver",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );

		}
	}

	@Test(priority = 13)

	public void test_AddProtectionPlan_Activity_9()  throws Exception{

		status =dtv_operation.add_protection_plan(driver, test, report);


		if (status) {
			test.pass("Protection Plan add validation Pass",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else {
			test.fail("Not able to add Protection Plan",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );

		}
	}



	@Test(priority = 14)
	public void test_AutoPay_BillHandle_Activity_11() throws Exception{

		boperaion =new BasicOperation(TestCase);
		status =boperaion.autoPay_PaperLessBillcheckBoxHandle(driver, test);

		if (status) {
			test.pass("AutoPay_BillHandle_Activity Succesfull",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else {
			test.fail("AutoPay_BillHandle_Activity fail",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
	}


	@Test(priority = 16)
	public void test_ValidateQuote_Activity_12() throws Exception
	{
		boperaion =new BasicOperation(TestCase);
		try {
			boperaion.validateQuote(driver);
			test.pass("Validate Quote done", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		catch(Exception e) {
			test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build());
		}
	}

	@Test(priority = 17)
	public void test_ClosePopUpButton_Activity_13() throws Exception{
		boperaion =new BasicOperation(TestCase);
		try {
			boperaion.check_ValidationMessage(driver, test);
			test.pass("Validation messgae handled", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		catch(Exception e) {
			test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build());
		}

	}

	@Test(priority = 18)
	public void test_click_continue_to_next_forward_Activity_12() throws Exception
	{
		boperaion =new BasicOperation(TestCase);
		try {
			boperaion.click_continue_to_next_forward(driver, test);;
			test.pass("Click continue to next page", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		catch(Exception e) {
			test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build());
		}
	}

	@Test(priority = 19)
	public void test_EnterCustomerDetails_Activity_12() throws Exception
	{
		info = new CreateAccount_Personal_Information(TestCase);
		status =info.setTextAccountHolderInformation(driver ,test);
		if (status) {

			test.pass("Credit check done, customer details entered ", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else{
			test.fail("Enter Customaer details failed", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build());
		}
	}


	@Test(priority = 20)
	public void test_AccountInformationUpdate() throws Exception{
		info = new CreateAccount_Personal_Information(TestCase);
		status = info.Account_Information(driver, test);
		if (status) {

			test.pass("Credit check done, Account deatils entered ", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else{
			test.fail("Account details failed", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build());
		}
	}

	@Test(priority = 21)
	public void test_Security_Information()  throws Exception{
		info = new CreateAccount_Personal_Information(TestCase);
		status = info.Security_Information(driver, test);
		if (status) {

			test.pass("Security_Information enter done", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else{
			test.fail("Security_Information enter failed", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build());
		}
	}
	
	@Test(priority = 22)
	public void test_Contact_Information_Click_Next()  throws Exception{
		info = new CreateAccount_Personal_Information(TestCase);
		status = info.Contact_Information(driver, test);
		if (status) {

			test.pass("Create account information enter done", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		}
		else{
			test.fail("Create account information enter failed", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build());
		}
	}
	
	@AfterTest
	public void teadDown() throws Exception {
		report_generator.take_ScreenShot(driver, TestCase);
		//				setup.teadDown(driver, report);
		setup.teadDown( report);
	}


}
