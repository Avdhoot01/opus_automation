package OpusAutomation.OpusAutomation;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

import basicFactor.ExtentReportGenerator;

public class DTV_VOIP_PR {

	WebDriver driver = null;
	
	
	static ExtentTest test;
	static ExtentReports report;
	static LaunchBrowser setup;
	boolean status = true;
	
	ExtentReportGenerator report_generator = new ExtentReportGenerator();
	static String TestCase ="PR DTV_VOIP";
	
	DTV_Basics_Operations dtv_operation;
	VOIP_Basic_Operation voip_operation;
	BasicOperation boperaion ;
	
	
	@BeforeTest
	public void setUp() throws Exception{
		List <Object> list_call= report_generator.create_Report(TestCase);

		test =(ExtentTest) list_call.get(0);
		report =(ExtentReports) list_call.get(1);

		setup = new LaunchBrowser("TEST_QC5");
		driver=setup.setUp(driver, test, report);

		System.out.println("Set Up");
	}
	
	

	@Test(priority = 1)
	public void test_StoreSelect_activity_1(){
		setup.StoreSelect(driver, test, report);
	}
	
	
	@Test(priority = 2)
	public void test_GlobanLogon_activity_2() throws Exception{
		test.info("Global Logon", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
					report_generator.take_ScreenShot(driver, TestCase)).build() );
		setup.globalLogon(driver, test, report);;
	}
	
	@Test(priority = 3)
	public void test_AddressValidation_activity_3() throws Exception{
		CreateContactAddressPage create_contact = new CreateContactAddressPage();
		status =create_contact.new_customer_click(driver,TestCase, test, report);

		if (status) {
			test.pass("Address Validation Pass");
		}
		else {
			test.fail("Address validation Failed");
			setup.teadDown( report);
//			System.exit(0);
			
		}
	}

	@Test(priority = 4)
	public void test_LOBSelect_Activity_4( ) throws Exception{

		ServiceProfileForNewCust selectLOB = new ServiceProfileForNewCust();
		status =selectLOB.serviceAvailability(driver, TestCase, test, report);


		if (status) {
			test.pass("LOB selected");
		}
		else {
			test.fail("LOB not selected");
			
		}

	}

	
	// ********************** DTV Validation ****************************
	
		@Test(priority = 5)

		public void test_DTV_BasePackageSelect_Activity_5()  throws Exception{

			dtv_operation = new DTV_Basics_Operations(TestCase);
			dtv_operation.Switch_TV(driver);
			status =dtv_operation.selectDTVPackage(driver, test, report);
			if (status) {
				test.pass("DTV Base Package selected",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build() );
			}
			else {
				test.fail("DTV Base Package not selected",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build() );
			}

		}

		@Test(priority = 6)
		public void test_check_to_add_promotion_Activity_6() throws Exception{
			dtv_operation.check_to_add_promotion(driver, "Y", test);
		}
		
		@Test(priority =7)

		public void test_TVAdd_Activity_7()  throws Exception{

			status =dtv_operation.number_of_tv_add(driver, test, report);


			if (status) {
				test.pass("TV added Succesfully",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build() );
			}
			else {
				test.fail("Not able to add TV",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build() );
			
			}

		}


		@Test(priority = 8)

		public void test_ReceiverAdd_Activity_8()  throws Exception{

			status =dtv_operation.add_reciver(driver, test, report);


			if (status) {
				test.pass("Reciever added Succesfully",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build() );
			}
			else {
				test.fail("Not able to add receiver",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build() );
				
			}
		}

		@Test(priority = 9)

		public void test_AddProtectionPlan_Activity_9()  throws Exception{

			status =dtv_operation.add_protection_plan(driver, test, report);


			if (status) {
				test.pass("Protection Plan add validation Pass",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build() );
			}
			else {
				test.fail("Not able to add Protection Plan",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build() );
				
			}
		}
	
		
		// **************************** VOIP Selection ******************//
		
		@Test(priority = 10)
		public void test_SelectVOIPPackage_Activity_10() throws Exception {
			
			
			try {
				voip_operation = new VOIP_Basic_Operation(TestCase);
				voip_operation.switch_to_VOIP(driver, test);
				
				status = voip_operation.Select_VOIP_Package(driver, test);
				
				if (status) {
					test.pass("VOIP Package Selected",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
							report_generator.take_ScreenShot(driver, TestCase)).build() );
				}
				else {
					test.fail("VOIP Package not Selected",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
							report_generator.take_ScreenShot(driver, TestCase)).build() );
				}
			}
			catch (Exception e) {
				test.fail(e,MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build() );
			}
			
		}
		
		@Test(priority = 11)
		public void test_AutoPay_BillHandle_Activity_11() throws Exception{
			
			boperaion =new BasicOperation(TestCase);
			status =boperaion.autoPay_PaperLessBillcheckBoxHandle(driver, test);
			
			if (status) {
				test.pass("AutoPay_BillHandle_Activity Succesfull",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build() );
			}
			else {
				test.fail("AutoPay_BillHandle_Activity fail",MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build() );
			}
		}
		
		
		@Test(priority = 12)
		public void test_ValidateQuote_Activity_12() throws Exception
		{
			try {
				boperaion.validateQuote(driver);
				test.pass("Validate Quote done", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build() );
			}
			catch(Exception e) {
				test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build());
			}
		}
		
		
		@Test(priority = 13)
		public void test_ClosePopUpButton_Activity_13() throws Exception{
			try {
				boperaion.check_ValidationMessage(driver, test);
				test.pass("Popup Closed", MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build() );
			}
			catch(Exception e) {
				test.fail(e, MediaEntityBuilder.createScreenCaptureFromPath(".\\Screenshots\\"+
						report_generator.take_ScreenShot(driver, TestCase)).build());
			}
			
		}
		@AfterTest
		public void teadDown() throws Exception {
			report_generator.take_ScreenShot(driver, TestCase);
//			setup.teadDown(driver, report);
			setup.teadDown( report);
		}
		
	
}
